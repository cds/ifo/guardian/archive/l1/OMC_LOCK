from guardian import GuardState, NodeManager, GuardStateDecorator

import time
import gpstime
import numpy as np
from scipy import signal
from matplotlib import mlab
import cmath
import cdsutils
import omcparams
#import ISC_library

from isclib.epics_average import EzAvg, ezavg


nodes = NodeManager(['SUS_OMC'])

##################################################
# Functions used only in OMC
##################################################

# A function for finding peaks above some threshold, scipy has find_peaks
# but it's not available in the current installed version of scipy.
# Also this is copied from scipy signal ......
def peak_finder(data,threshold=0):

    ind_peaks = []

    i = 1  # Pointer to current sample, first one can't be maxima
    i_max = data.shape[0] - 1  # Last sample can't be maxima

    while i < i_max:
        # Test if previous sample is smaller
        if data[i - 1] < data[i]:
            i_ahead = i + 1  # Index to look ahead of current sample

            # Find next sample that is unequal to x[i]
            while i_ahead < i_max and data[i_ahead] == data[i]:
                i_ahead += 1

            # Maxima is found if next unequal sample is smaller than x[i]
            if data[i_ahead] < data[i]:
                left_edge = i
                right_edge = i_ahead - 1
                midpoint = (left_edge + right_edge) // 2
                if data[midpoint]>threshold:
                    ind_peaks.append(midpoint)
                # Skip samples that can't be maximum
                i = i_ahead
        i += 1

    ind_peaks = np.array(ind_peaks)

    return ind_peaks


def sb_finder(ind_maxima,pztVolt,dcpdSum):

    dummy_2 = ind_maxima
    dummy_1 = np.delete(ind_maxima,len(ind_maxima)-1)

    ind_SB = []

    for j in dummy_1:
    # recursively remove the first element to avoid double comparison
        dummy_2 = np.delete(dummy_2,0)
        for k in dummy_2:
            pzt_diff = pztVolt[k]-pztVolt[j]
            dcpd_ratio = dcpdSum[j]/dcpdSum[k]
            dcpd_mean = np.mean([dcpdSum[j],dcpdSum[k]])

            if ((12.0 < abs(pzt_diff) < 18.0) and (0.5 < dcpd_ratio < 2.0) and (dcpd_mean>30.0)): 
                # and max_dcpd[j] > dcpd_threshold):
                # Peaks are found, record indices:
                log('Found the sidebands!')
                # FIXME: make arrays to catch any other peaks?
                ind_SB.append(j)
                ind_SB.append(k)
                log("First Sideband is at {0:.2f} volts, second sideband is at {1:.2f} volts".format(pztVolt[j],pztVolt[k]))
                break

    return ind_SB


def whitening_change_helper(dcpd_filt, dcpd_stages, action='add'):
    if action == 'add':
        # Find the lowest-numbered stage that is not on,
        # and then turn it on
        for stagenum, stage in enumerate(dcpd_stages):
            if ezca[stage] != 1:
                ezca[stage] = 1
                dcpd_filt.switch('FM{}'.format(stagenum+1), 'ON')
                break
    if action == 'remove':
        # Find the highest-numbered stage that is on,
        # and then turn it off
        for stagenum, stage in enumerate(dcpd_stages[::-1]):
            stagenum = len(dcpd_stages)-1-stagenum
            if ezca[stage] != 0:
                ezca[stage] = 0
                dcpd_filt.switch('FM{}'.format(stagenum+1), 'OFF')
                break

#####
def turn_off_omc_asc():

    # Ramp down ASC master gain:
    #while ezca['OMC-ASC_MASTERGAIN'] > 0.0:
    #    ezca['OMC-ASC_MASTERGAIN'] -= 0.05
    #    time.sleep(0.1)
    ezca['OMC-ASC_MASTERGAIN'] = 0.0
    time.sleep(0.1)
    ezca['OMC-ASC_QDSLIDER'] = 0.0

    # Gracefully clear histories
    val={'TRAMP':5,'GAIN':0,'RSET':2}
    tsleep={'TRAMP':0.0,'GAIN':0.1,'RSET':5}
    for aspect in ['TRAMP','GAIN','RSET']:
        time.sleep(tsleep[aspect])
        for dof in ['X','Y']:
            for basis in ['ANG','POS']:
                ezca['OMC-ASC_'+basis+'_'+dof+'_'+aspect] = val[aspect]

    # Turn off integrators
    for dof in ['X','Y']:
        for basis in ['ANG','POS']:
            ezca.switch('OMC-ASC_'+basis+'_'+dof,'FM2','OFF')
    
    # Set gains back to nominal
    val={'TRAMP':0,'GAIN':omcparams.asc_gain}
    tsleep={'TRAMP':0.0,'GAIN':0.1}
    for aspect in ['TRAMP','GAIN']:
        time.sleep(tsleep[aspect])
        for dof in ['X','Y']:
            for basis in ['ANG','POS']:
                ezca['OMC-ASC_'+basis+'_'+dof+'_'+aspect] = val[aspect]

#####
def unlock_omc():

    #FIXME: Should the gain be ramped down before switching off integrator?
    # Turn off LSC servo
    #ezca['OMC-LSC_SERVO_TRAMP'] = 3
    #time.sleep(0.1)
    #ezca['OMC-LSC_SERVO_GAIN'] = 0
    #time.sleep(3)

    ezca.switch('OMC-LSC_SERVO','FM2','OFF')
    time.sleep(0.1)
    ezca.switch('OMC-LSC_SERVO','FM1','INPUT','OFF')
    time.sleep(0.1)
    ezca['OMC-LSC_SERVO_RSET'] = 2
    time.sleep(0.1)

    #ezca['OMC-LSC_SERVO_GAIN'] = omcparams.lsc_gain

#####
def reset_pzt():
    ezca['OMC-PZT2_TRAMP'] = 10
    time.sleep(0.2)
    ezca['OMC-PZT2_OFFSET'] = ezca['OMC-STORE_PZT2_OFFSET'] - 2.5
    time.sleep(0.2)
    ezca['OMC-PZT2_TRAMP'] = 0.1
    
#####
def light_on_qpds():
    sumA = 0.0
    sumB = 0.0
    for i in range(11):
        sumA += ezca['ASC-OMC_A_SUM_OUT16']
        sumB += ezca['ASC-OMC_B_SUM_OUT16']
        time.sleep(0.1)

    qpda_sum = sumA/11
    qpdb_sum = sumB/11

    return (qpda_sum>omcparams.qpd_power_threshold and qpdb_sum>omcparams.qpd_power_threshold)

#####
def ready_for_dc():

    omc_dcpd = cdsutils.avg(4, 'OMC-DCPD_SUM_OUTPUT', stddev=False)
    #omc_dcpd = ezavg(ezca, 3.0, 'OMC-DCPD_SUM_OUTPUT')
        
    return (ezca.LIGOFilter('LSC-DARM').is_engaged('OFFSET') and
        ezca.LIGOFilter('OMC-LSC_SERVO').is_input_on()       and
        ezca.LIGOFilter('OMC-LSC_SERVO').is_engaged('FM1')   and
        ezca.LIGOFilter('OMC-LSC_SERVO').is_engaged('FM2')   and
        ezca['OMC-ASC_MASTERGAIN'] == 1                      and
        ezca.LIGOFilter('OMC-ASC_POS_X').is_engaged('FM2')   and
        ezca.LIGOFilter('OMC-ASC_POS_Y').is_engaged('FM2')   and
        ezca.LIGOFilter('OMC-ASC_ANG_X').is_engaged('FM2')   and
        ezca.LIGOFilter('OMC-ASC_ANG_Y').is_engaged('FM2')   and
        (3.0 < omc_dcpd < 13))


##################################################
# DECORATORS: 
##################################################

class assert_light_on_qpds(GuardStateDecorator):
    """Decorator that jumps to DOWN state if no light on qpds."""
    def pre_exec(self):
        #if nodes['SUS_OMC'] != 'ALIGNED':
        #    notify('OMC suspension is not aligned')
        #    return 'DOWN'
        if not light_on_qpds():
            log('No QPD light! Returning to WAIT_FOR_AS_LIGHT...')
            unlock_omc()
            turn_off_omc_asc()
            return 'WAIT_FOR_AS_LIGHT'

class assert_omc_locked(GuardStateDecorator):
    def pre_exec(self):
        if (ezca['OMC-LSC_LOCK_TRIGGER_MON'] == 0):
            unlock_omc()
            turn_off_omc_asc()
            reset_pzt()
            return 'DOWN'
            #return 'FAULT'

##################################################
# STATES: 
##################################################
nominal = 'READY_FOR_HANDOFF'


###########################################################
class INIT(GuardState):
    index = 0
    request = True

# Removed by Stuart A - Jan 7th 2020 - Try to reduce SUS_OMC from getting stuck 
#    def main(self):
#        nodes.set_managed()

    def run(self):
        #if nodes['SUS_OMC'] != 'ALIGNED':
        #    notify('OMC suspension is not aligned')
        #    return 'FAULT'
        return 'IDLE'
        #return 'DOWN'

###########################################################
# FIXME: I don't understand the point of this state, seems like it's simply
# for indicating when SUS_OMC is not aligned, but the DOWN state does this too
# I'm commenting it out for now - A. Mullavey 2020-02-26
'''
class FAULT(GuardState):
    request = False
    redirect = False

    def main(self):
        # LSC switches
        ezca.switch('OMC-LSC_SERVO','IN','FM1','FM2','OFF')     
        #ezca['OMC-LSC_SERVO_GAIN'] = 0
        #servofilt = ezca.get_LIGOFilter('OMC-LSC_SERVO')
        #servofilt.only_on('LIMIT', 'OUTPUT', 'DECIMATION', *omcparams.lsc_filters)

        # ASC switches
        ezca['OMC-ASC_MASTERGAIN'] = 0
        #ezca['OMC-ASC_QDSLIDER'] = 1
        for filt in ['POS_X', 'POS_Y', 'ANG_X', 'ANG_Y']:
            ezca.switch('OMC-ASC_' + filt, 'FM2', 'OFF')

    def run(self):
        if nodes['SUS_OMC'] != 'ALIGNED':
            notify('OMC suspension is not aligned')
            return True
        return 'DOWN'
'''

class IDLE(GuardState):
    index = 10
    request = True
    goto = True

    def run(self):
        return True

class DOWN(GuardState):
    index = 100
    request = True
    goto = True

    def main(self):

        unlock_omc()
        turn_off_omc_asc()
        reset_pzt()

        #servofilt = ezca.get_LIGOFilter('OMC-LSC_SERVO')
        #servofilt.only_on('LIMIT', 'OUTPUT', 'DECIMATION', *omcparams.lsc_filters)
        #ezca.switch('OMC-DCPD_NORM_FILT','INPUT','ON')
        #ezca['OMC-LSC_LOCK_TRIGGER_THRESHOLD'] = omcparams.lsc_trigger_threshold

    def run(self):
        if not nodes['SUS_OMC'] == 'ALIGNED':
            notify('OMC suspension is not aligned')
            return

        return True

###########################################################
class OFF_RESONANCE(GuardState):
    index = 3
    request = True

    def main(self):
        self.timer['pause'] = 1

    def run(self):
        if nodes['SUS_OMC'] == 'ALIGNED':
            if self.timer['pause']:
                self.timer['pause'] = 1
                if ezca['OMC-DCPD_SUM_OUT16'] > 0.02:
                    if ezca['OMC-PZT2_OFFSET'] > 50:
                        ezca['OMC-PZT2_OFFSET'] = -50
                    else:
                        ezca['OMC-PZT2_OFFSET'] += 1           
            return True
        else:
            notify('OMC suspension is not aligned')


###########################################################
class WAIT_FOR_AS_LIGHT(GuardState):
    index = 110
    request = False

    def main(self):

        self.timer['wait'] = 0

    def run(self):

        #return True	#Skip this state if we accidentally end up here	#AJM20200204

        #FIXME: Do we need this. Fast shutter is unblocked in ISC LOCK
        #if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
        #    notify('Toast is ready!')
        #    ezca['ISI-HAM6_WD_RSET'] = 1
        #    time.sleep(0.5)
        #    ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter

        if ready_for_dc():
            return 'MANUAL_LOCKING'

        if not self.timer['wait']:
            return

        if not (nodes['SUS_OMC'] == 'ALIGNED'):
            notify('OMC suspension is not aligned')
            return

        if not light_on_qpds():
            notify('Not enough light on OMC QPDs. Waiting...')
            self.timer['wait'] = 2
            return

        return True

###########################################################
class ASC_QPD_ON(GuardState):
    index = 120
    request = True

    @assert_light_on_qpds
    def main(self):
        #turn on QPD loops
        ezca['OMC-ASC_MASTERGAIN'] = 0
        ezca['OMC-ASC_QDSLIDER'] = 1.0 

        self.filts = []
        for dof1 in ['POS','ANG']:
            for dof2 in ['X','Y']:
                self.filts.append("OMC-ASC_{}_{}".format(dof1,dof2))

        # Set OMC ASC filter gains FIXME: maybe unnecessary
        for filt in self.filts:
            ezca.write("{}_GAIN".format(filt),6)

        # Ramping master gain up to 0.1	#FIXME: Check what gain works.
        self.asc_step = cdsutils.Step(ezca, 'OMC-ASC_MASTERGAIN','+0.02,5',time_step=0.2)
        self.integrators = True

        self.err_chans = []
        for filt in self.filts:
            self.err_chans.append("{}_INMON".format(filt))

    @assert_light_on_qpds
    def run(self):

        if not self.asc_step.step():
            return

        if self.integrators:
            #for filt in ['POS_X', 'POS_Y', 'ANG_X', 'ANG_Y']:
            #    ezca.switch('OMC-ASC_' + filt, 'FM2', 'ON')
            for filt in self.filts:
                ezca.switch(filt, 'FM2', 'ON')
            self.integrators = False

        #TODO: make sure error signals go to zero
        err_sigs = cdsutils.avg(1,self.err_chans)
        if any(abs(err)>0.01 for err in err_sigs):
            log('Waiting for error signals to zero')
            return

        return True

###########################################################
class PREP_OMC_SCAN(GuardState):
    index = 140
    request = False

    #@assert_light_on_qpds
    #def main(self):
    #    
    #    # initialize the PZT settings 
    #    ezca['OMC-PZT2_TRAMP'] = 10
    #    ezca['OMC-PZT2_OFFSET'] = omcparams.scan_vstart 
    #    self.timer['sleep'] = 10

    @assert_light_on_qpds
    def run(self):
        #FIXME: JCB 2018/06/06
        return True
        #if self.timer['sleep']:
        #    # check if DARM offset is resonable 
        #    darm_offset = ezca['LSC-DARM1_OFFSET']
        #    if abs(darm_offset)< omcparams.darm_offset_LOLO or abs(darm_offset)>omcparams.darm_offset_HIHI:
        #      notify('DARM offset is not set properly?? Check LSC-DARM1_OFFSET.')
        #        self.timer['sleep'] = 1 # sleep until somebody activates DARM offset
        #    else:
        #        return True    

###################################################################
#
# Lock the OMC on the carrier with a search around previous stored pzt value  CB added 22Feb 2024
class OMC_QUICKLOCK(GuardState):
    index = 145
    request = False

    @assert_light_on_qpds
    def main(self):

        self.pztStoredChan = float(ezca['OMC-STORE_PZT2_OFFSET'])
        # Turn offset on and set to 8
        ezca.switch('LSC-DARM','OFFSET','ON')
        #ezca['LSC-DARM_OFFSET'] += 1.0
        ezca['LSC-DARM_OFFSET'] = 5 # looses it engaging integrator if the offset is too large 
        # Clear servo history
        ezca['OMC-LSC_SERVO_RSET']=2
        time.sleep(0.3)
        # Move PZT to old stored value
        ezca['OMC-PZT2_TRAMP'] = 3 
        pzt_range = 4.0 # restore stored value is from previous lock
        ezca['OMC-PZT2_OFFSET'] = self.pztStoredChan - 2
        time.sleep(3)
	# Set Tramp
        ezca['OMC-PZT2_TRAMP'] = 0.1
        # Set locking trigger thresholds
        ezca['OMC-LSC_LOCK_TRIGGER_THRESH_ON'] = 4.0
        ezca['OMC-LSC_LOCK_TRIGGER_THRESH_OFF'] = 1.0
        time.sleep(0.2)
        # Engage LSC Filter Input and Boost, Int off, won't lock until triggered
        ezca.switch('OMC-LSC_SERVO','FM1','INPUT','ON','FM2','OFF')

        self.carrier_check = True
        self.counter = 0
        self.nofind_carrier = False
        self.lock_count = 0

        self.pzt_step = 0.02
        self.n_steps = pzt_range/self.pzt_step

        self.pzt_start = ezca['OMC-PZT2_OFFSET']

        self.try_count = 0

        self.offset_init = ezca['LSC-DARM_OFFSET']


    #@assert_light_on_qpds
    def run(self):

        if self.nofind_carrier:
            self.try_count += 1
            if self.try_count < 2:
                # Turn off integrator and then boost and input
                unlock_omc()
                log("Lock failed on attempt {0}, trying again".format(self.try_count))
                ezca['OMC-PZT2_TRAMP'] = 3
                time.sleep(0.3)
                ezca['OMC-PZT2_OFFSET'] = self.pzt_start
                time.sleep(3)
                ezca['OMC-PZT2_TRAMP'] = 0.1
                ezca.switch('OMC-LSC_SERVO','FM1','INPUT','ON','FM2','OFF')  # Input and boost on, integrator off
                self.counter = 0
                self.nofind_carrier = False
            else:
                log('Carrier not found, attempting slow scan in FIND_CARRIER')
                # reset setting to those going in to FIND_CARRIER
                unlock_omc()
                #turn_off_omc_asc() # Do not if going to FIND_CARRIER
                #reset_pzt()  # Do not if going to FIND_CARRIER
                ezca['OMC-PZT2_OFFSET'] = self.pztStoredChan
                ezca['LSC-DARM_OFFSET'] = 5
                ezca.switch('LSC-DARM','OFFSET','OFF')  # Done in FIND_CARRIER
                ezca.switch('OMC-LSC_SERVO','FM1','INPUT','OFF','FM2','OFF')  # OMC locking off
                return 'FIND_CARRIER'

        #Check that DARM OFFSET is on
        if not ezca.LIGOFilter('LSC-DARM').is_engaged('OFFSET'):
            notify('DARM offset is not on')
            return
        # Nominal is 7.0
        if not 3.5<ezca['LSC-DARM_OFFSET']<9.1:
        #if not 6.4<ezca['LSC-DARM_OFFSET']<9.1:
            notify('DARM offset is not correct value')
            self.nofind_carrier = True
            return

        # Step pzt until the cavity locks. Should only step it for 5V, if carrier not found do full scan .
        if not ezca['OMC-LSC_LOCK_TRIGGER_MON']:
            ezca['OMC-PZT2_OFFSET'] += self.pzt_step#*self.sign
            self.counter += 1
            if self.counter >= self.n_steps:
                log('Did not lock on carrier, increasing DARM offset and trying again')
                ezca['LSC-DARM_OFFSET'] += 1.0
                self.nofind_carrier = True
            time.sleep(0.1)	#Just for testing
            return

        # Do carrier check
        if self.carrier_check:
            #Engage integrator
            log('Checking appropriate current')
            omc_dcpd = cdsutils.avg(3, 'OMC-DCPD_SUM_OUTPUT', stddev=False)
            if 4.0 < omc_dcpd < 13.0:
                #if not ezca['OMC-LSC_LOCK_TRIGGER_MON']:
                #    self.nofind_carrier = True
                #    log('Not locked on the carrier')
                #    return
                log('Engaging integrator (FM2)')
                ezca.switch('OMC-LSC_SERVO','FM2','ON')
                time.sleep(1)
                log('Locked on the carrier')
                #ezca['OMC-LSC_LOCK_TRIGGER_THRESHOLD'] = 1.0
                self.carrier_check = False
            else:
                log('Maybe not the carrier')
                if omc_dcpd > 13.0:
                    ezca['LSC-DARM_OFFSET'] -= 1.0
                if omc_dcpd < 4.0:
                    ezca['LSC-DARM_OFFSET'] += 1.0
                self.nofind_carrier = True
                return

        return True


###########################################################
class FIND_CARRIER(GuardState):
    index = 150
    request = True

    @assert_light_on_qpds
    def main(self):

        #Parameters
        self.sample_rate = 16.0		#Update Rate set to Epics rate (Hz)
        self.T_scan = 45.0		#Scan duration (s)
        self.pztRange = 25.0

        self.g_offset = 327.68		#Gain of PZT2 bank

        # Channels
        self.pztOffetChan = 'OMC-PZT2_OFFSET'
        self.pztTrampChan = 'OMC-PZT2_TRAMP'
        self.pztStoredChan = 'OMC-STORE_PZT2_OFFSET'
        self.pztOutChan = 'OMC-PZT2_OUTPUT'
        self.pztVoltChan = 'OMC-PZT2_MON_DC_OUTPUT'
        self.dcpdSumChan = 'OMC-DCPD_SUM_OUTPUT'


        self.pztOutArray = np.array([])
        self.pztVoltArray = np.array([])
        self.dcpdSumArray = np.array([])

        # Clear LSC_OMC history
        ezca['OMC-LSC_SERVO_RSET'] = 2

        # Turn DARM offset off, take away carrrier, less things to mistake for a sideband
        ezca.switch('LSC-DARM','OFFSET','OFF')

        # Initialize timers
        self.timer['InitMove'] = 0
        self.timer['Other'] = 0
        
        # Set Flags
        self.move_to_start = True
        self.start_scan = False
        self.find_SBs = False

        # Store Scan Data?
        self.store_scandata = True
        #self.filedir = '/opt/rtcds/userapps/trunk/omc/l1/guardian/scan_data/'
        self.filedir = '/data/OMC/grd_scans/'
        
        # A counter for the scan
        self.scan_count = 0

        self.gtime = gpstime.gpsnow()

    #@assert_light_on_qpds	#decorators slow down the data grab
    def run(self):

        # Move PZT to starting position
        if self.move_to_start:
            t_init = 10.0
            ezca[self.pztTrampChan] = t_init
            time.sleep(0.1)
            ezca[self.pztOffetChan] = ezca[self.pztStoredChan] - self.pztRange/2
            self.timer['InitMove'] = t_init + 1.0
            self.timer['Other'] = 0
            self.start_scan = True
            self.move_to_start = False

        if not self.timer['InitMove']:                         
            log('Moving to starting position')
            return

        if not self.timer['Other']:
            return

        # Scan the PZT
        if self.start_scan:
            ezca['OMC-PZT2_TRAMP'] = self.T_scan
            time.sleep(0.1)
            self.t_start = int(gpstime.gpsnow())	# Grab current time
            log("Scan started at {}".format(self.t_start))
            ezca['OMC-PZT2_OFFSET'] += self.pztRange
            self.start_scan = False
            self.find_SBs = True
            self.scan_count += 1

        if (gpstime.gpsnow()-self.t_start)<self.T_scan:
            self.dcpdSumArray = np.append(self.dcpdSumArray,ezca[self.dcpdSumChan])
            self.pztOutArray = np.append(self.pztOutArray,ezca[self.pztOutChan]/self.g_offset)
            self.pztVoltArray = np.append(self.pztVoltArray,ezca[self.pztVoltChan])
            return

        # Store in file? Mainly for debugging purposes
        if self.store_scandata:
            log("Storing Data")
            filename = self.filedir + 'omcScan_'+str(self.t_start)+'.npz'
            np.savez(filename, pztOut=self.pztOutArray, pztVolt=self.pztVoltArray, dcpdSum=self.dcpdSumArray)
            self.store_scandata = False 

        # Find SBs and move to the center minus 5V (for hysteresis)
        if self.find_SBs:
            self.find_SBs = False
            #Find the indices of local maxima
            log('Finding Indices of Peaks')
            ind_maxima = peak_finder(self.dcpdSumArray,threshold=5)
            #Find the 45MHz sideband indices
            log('Calculating where the sidebands are.')
            #ind_SB = []
            ind_SB = sb_finder(ind_maxima,self.pztOutArray,self.dcpdSumArray)
            if len(ind_SB)<2:
                log('Sidebands not found')
                log("Sideband finder code returned {0}".format(ind_SB))
                if self.scan_count < 2:
                    log('Doubling range and scanning again')
                    self.pztRange *= 2
                    self.T_scan *=2
                    self.pztOutArray = np.array([])
                    self.pztVoltArray = np.array([])
                    self.dcpdSumArray = np.array([])
                    self.move_to_start = True
                    self.store_scandata = True
                    return
                else:
                    log('Both scans failed, jumping to Manual Locking')
                    reset_pzt()
                    turn_off_omc_asc()
                    return 'MANUAL_LOCKING'
            #Estimate where the Carrier will be
            offset_carrier_est = (self.pztOutArray[ind_SB[0]] + self.pztOutArray[ind_SB[1]])/2 - 6.0
            #Send the OMC PZT to the Carrier Resonance Spot
            ezca['OMC-PZT2_TRAMP'] = 10
            time.sleep(0.3)
            ezca['OMC-PZT2_OFFSET'] = round(offset_carrier_est,2)
            log('Moving OMC PZT near the Carrier Resonance')
            self.timer['Other'] = 12
            return

        #TODO: If sideband power is low (nominally 16A), lock on sideband and engage ASC

        return True


###########################################################

# Lock the OMC on the carrier TEM00 resonance.  
class OMC_LSC_ON(GuardState):
    index = 200
    request = False

    @assert_light_on_qpds
    def main(self):

        # Turn offset on
        ezca.switch('LSC-DARM','OFFSET','ON')
        # Clear servo history
        ezca['OMC-LSC_SERVO_RSET']=2
        time.sleep(0.3)
        # Set Tramp
        ezca['OMC-PZT2_TRAMP'] = 0.1
        # Set locking trigger threshold higher (a schmidt trigger would be nice here)
        #ezca['OMC-LSC_LOCK_TRIGGER_THRESHOLD'] = 3.5	#3.0
        ezca['OMC-LSC_LOCK_TRIGGER_THRESH_ON'] = 4.0
        ezca['OMC-LSC_LOCK_TRIGGER_THRESH_OFF'] = 1.0
        time.sleep(0.2)
        # Engage LSC Filter Input and Boost, Int off, won't lock until triggered
        ezca.switch('OMC-LSC_SERVO','FM1','INPUT','ON','FM2','OFF')

        self.carrier_check = True
        self.counter = 0
        self.nofind_carrier = False
        self.lock_count = 0

        pzt_range = 6.0 #3.5
        self.pzt_step = 0.02
        self.n_steps = pzt_range/self.pzt_step

        self.pzt_start = ezca['OMC-PZT2_OFFSET']

        self.try_count = 0

        self.offset_init = ezca['LSC-DARM_OFFSET']

        # If taking too long
        #self.timer['timeout'] = 100


    #@assert_light_on_qpds
    def run(self):

        if self.nofind_carrier:
            self.try_count += 1
            if self.try_count < 3:
                # Turn off integrator and then boost and input
                unlock_omc()
                log("Lock failed on attempt {0}, trying again".format(self.try_count))
                ezca['OMC-PZT2_TRAMP'] = 5
                time.sleep(0.3)
                ezca['OMC-PZT2_OFFSET'] = self.pzt_start
                time.sleep(5)
                ezca['OMC-PZT2_TRAMP'] = 0.1
                ezca.switch('OMC-LSC_SERVO','FM1','INPUT','ON','FM2','OFF')
                self.counter = 0
                self.nofind_carrier = False
            else:
                log('Carrier not found, please lock manually')
                unlock_omc()
                #turn_off_omc_asc()
                reset_pzt()
                ezca['LSC-DARM_OFFSET'] = self.offset_init
                return 'MANUAL_LOCKING'

        #Check that DARM OFFSET is on
        if not ezca.LIGOFilter('LSC-DARM').is_engaged('OFFSET'):
            notify('DARM offset is not on')
            return
        # Nominal is 5.0
        if not 3.5<ezca['LSC-DARM_OFFSET']<9.1:
            notify('DARM offset is not correct value')
            log('Carrier not found within DARM OFFSET range 3.5-9.1, please lock manually')
            unlock_omc()
            reset_pzt()
            ezca['LSC-DARM_OFFSET'] = self.offset_init
            return 'MANUAL_LOCKING'                        # CB clean up and send to manual locking as we got stuck here 5 Mar 24

        # Tally up the iterations for when the OMC LOCK is triggered, reset to zero when not.
        #if ezca['OMC-LSC_LOCK_TRIGGER_LOCKMON']:
        #    self.lock_count += 1
        #else:
        #    self.lock_count = 0 

        # Step pzt until the cavity locks. Should only step it for 5V, repeat 3 times and then throw an error.
        #if self.lock_count < 3:
        if not ezca['OMC-LSC_LOCK_TRIGGER_MON']:
            ezca['OMC-PZT2_OFFSET'] += self.pzt_step#*self.sign
            self.counter += 1
            if self.counter >= self.n_steps:
                #if self.reverse:
                #    self.sign = -1
                #    self.counter = 0.0
                #    self.reverse = False
                #    return
                log('Did not lock on carrier, increasing DARM offset and trying again')
                ezca['LSC-DARM_OFFSET'] += 1.0
                self.nofind_carrier = True
            time.sleep(0.1)	#Just for testing
            return

        #TODO: Have PZT finish sweep even if find wrong mode. Maybe have it scan back


        # Offload the control signal
        #if self.offload:
        #    ezca['OMC-PZT2_TRAMP'] = 2
        #    ctrl = cdsutils.avg(3, 'OMC-LSC_SERVO_OUT16')
        #    ezca['OMC-PZT2_OFFSET'] += round(ctrl,4)
        #    time.sleep(2)
        #    self.offload = False
        #    return


        # Do a carrier check
        if self.carrier_check:
            #Engage integrator
            log('Engaging integrator (FM2)')
            ezca.switch('OMC-LSC_SERVO','FM2','ON')
            time.sleep(1)
            log('Checking if carrier')
            omc_dcpd = cdsutils.avg(3, 'OMC-DCPD_SUM_OUTPUT', stddev=False)
            #omc_dcpd = ezavg(ezca,2.0,'OMC-DCPD_SUM_OUTPUT')
            if 4.0 < omc_dcpd < 13.0:
                log('Locked on the carrier')
                #ezca['OMC-LSC_LOCK_TRIGGER_THRESHOLD'] = 1.0
                self.carrier_check = False
            else:
                log('Maybe not the carrier')
                if omc_dcpd > 13.0:
                    ezca['LSC-DARM_OFFSET'] -= 1.0
                if omc_dcpd < 4.0:
                    ezca['LSC-DARM_OFFSET'] += 1.0
                self.nofind_carrier = True
                return

        return True



###########################################################
# Requestable state with OMC resonating before handoff
class OMC_LOCKED(GuardState):
    index = 250
    request = True

    #def main(self):
    	#Increase DARM offset by one and check the DCPD goes up by approximately the correct amount
        #self.lockamp = cdsutils.avg(3,'OMC-DCPD_SUM_OUTPUT',stddev=False)
        #self.expectratio = 1 + 0.75/ezca['LSC-DARM_OFFSET']
        #ezca['OMC-PZT2_TRAMP'] = 1
        #ezca['OMC-PZT2_OFFSET'] += 1
        #time.sleep(1)
        #@assert_light_on_qpds
        #@assert_omc_locked
    def run(self):
        #if cdsutils.avg(3,'OMC-DCPD_SUM_OUTPUT',stddev=False)>self.lockamp*self.expectratio:
        #    return True
        #else:
        #    notify('wrong mode - RF sideband')
        #    return 'DOWN'
        return True


###########################################################
# Switch on dither alignment
class ASC_DITHER_ON(GuardState):
    index = 350
    request = True

    #@assert_light_on_qpds
    @assert_omc_locked
    def main (self):

        # If not on dither, switch to dither
        if not ezca['OMC-ASC_QDSLIDER']==0.0:
            self.rampToDither = True
        else: 
            self.rampToDither = False

        self.rampAscMaster = True
        self.integrators = True

    #@assert_light_on_qpds
    @assert_omc_locked
    def run(self):
        # turn on asc gain and integrators

        if self.rampToDither:
            if ezca['OMC-ASC_QDSLIDER'] > 0.0:
                ezca['OMC-ASC_QDSLIDER']-=0.05
                time.sleep(0.05)
                return
            ezca['OMC-ASC_QDSLIDER'] = 0.0
            self.rampToDither = False

        if self.rampAscMaster:
            if ezca['OMC-ASC_MASTERGAIN'] < 1.0:
                #log('Ramping on ASC Master Gain')
                ezca['OMC-ASC_MASTERGAIN']+=0.05
                time.sleep(0.05)
                return
            ezca['OMC-ASC_MASTERGAIN'] = 1.0
            self.rampAscMaster = False

        if self.integrators:
            for filt in ['POS_X', 'POS_Y', 'ANG_X', 'ANG_Y']:
                ezca.switch('OMC-ASC_' + filt, 'FM2', 'ON')
            self.integrators = False

        return True

################################################################3
# Check Carrier - state added to check the quicklock state does not end up on a sideband HOM
class CARRIER_CHECK(GuardState):
    index = 400
    request = True

    def main(self):
    	#Increase DARM offset by one and check the DCPD goes up by approximately the correct amount
        self.lockamp = cdsutils.avg(3,'OMC-DCPD_SUM_OUTPUT',stddev=False)
        self.step = 2
        self.expectratio = 1+self.step/2/ezca['LSC-DARM_OFFSET']
        self.pztStoredChan = float(ezca['OMC-STORE_PZT2_OFFSET'])
        ezca['LSC-DARM_TRAMP'] = 1
        ezca['LSC-DARM_OFFSET'] += self.step
        time.sleep(1)
        #@assert_light_on_qpds
        #@assert_omc_locked
    def run(self):
        if cdsutils.avg(3,'OMC-DCPD_SUM_OUTPUT',stddev=False)>self.lockamp*self.expectratio:
            ezca['LSC-DARM_OFFSET'] -= self.step
            time.sleep(1)
            return True
        else:
            notify('wrong mode - RF sideband')
            # Set up to go back to find carrier
            unlock_omc()
            #ezca.switch('OMC-LSC_SERVO','FM1','INPUT','OFF','FM2','OFF')  # OMC locking off - done in unlock_omc()
            ezca['OMC-PZT2_OFFSET'] = self.pztStoredChan - 2.7
            ezca.switch('LSC-DARM','OFFSET','OFF')  # Also done in FIND_CARRIER
            return 'FIND_CARRIER'

###########################################################
# A state for manual locking. Hopefully we can eliminate this soon!
class MANUAL_LOCKING(GuardState):
    index = 450
    request = True

    def main(self):

        #Ensure everything gets reset
        ezca['OMC-LSC_LOCK_TRIGGER_THRESH_OFF'] = 0.03
        ezca['OMC-LSC_LOCK_TRIGGER_THRESH_ON'] = 1.0
        ezca.switch('LSC-DARM','OFFSET','ON')

    def run(self):

        notify('Manual Locking Required or request DOWN and try auto again')

        if not ready_for_dc():
            return

        return True


##############################################################################################

# Check that offsets and scale factor for LSC-OMC_DC are set for the handoff to DC readout
#
# Three things need to happen before the DARM handoff from ASQ --> DC readout can occur:
#       1) The power normalization has to be accounted for in the LSC input matrix.  
#          This is handled by the ISC_LOCK guardian.
#
#       2) A gain setting must be adjusted to match LSC-OMC_DC to the input to DARM; 
#          this is a measure of the sensitivity of DCPD_SUM to DARM length changes and 
#          varies as a function of the DARM offset.  This is done by changing 
#          H1:OMC-READOUT_SCALE_OFFSET so that the ratio of 
#          LSC-DARM_IN1 / LSC-OMC_DC_OUT is around 1 (+/- 5 percent) in a frequency 
#          band where the coherence is good, usually between 1-10Hz.
#          See the following DTT template: /ligo/home/daniel.hoak/dtt/OMC/OMC_DARM_handoff.xml.
#          Also the relative phase should be zero or you will not go to DC readout today.
#
#       3) An offset must be adjusted so that LSC-OMC_DC has the same overall value as the input to DARM.  
#          This is done by tuning LSC-OMC_DC_OFFSET
#          (which comes after the gain that was adjusted in #2) so that the average value of 
#          LSC-OMC_DC_OUT matches LSC-DARM_IN1.
#

'''
class TUNE_OFFSETS(GuardState):
    index = 400
    request = True

    @assert_light_on_qpds  
    #@ISC_library.assert_dof_locked_gen(['OMC'])
    @assert_omc_locked
    def main(self):
        # Set up the OMC-READOUT path.  Turn off the SIMPLE scaling path, enable the ERR path, make sure the X0 offset gain is 1.
        # Set the LSC-OMC_DC_OFFSET to a reasonable value, this will be changed later
        ezca['OMC-READOUT_SIMPLE_GAIN'] = 0
        ezca['OMC-READOUT_ERR_GAIN'] = 1
        ezca['OMC-READOUT_X0OFFSET_GAIN'] = 1
        
        GPS_start = ezca['FEC-8_TIME_DIAG']
        log(GPS_start)

        log('Fetching DARM-OMC TF Data')
        Tdata = 25
        time.sleep(Tdata-5)
       
        # Read in DARM_IN1 and OMC-DCPD_SUM so we can match gains
        # We want to match the gain using OMC-READOUT_ERR_GAIN, so turn the crank on 
        #   OMC-DCPD_SUM to work through the READOUT_ERR path calculations

        z = cdsutils.getdata(['L1:LSC-DARM_IN1_DQ','L1:OMC-DCPD_SUM_OUT_DQ'],Tdata,GPS_start-8)
        DARM_IN = z[0].data

        # Set the PREF offset so that the DCPD Sum is properly normalized
        # P_AS * Pref / P0 = (x0**2 / xf**2)
        # --> Pref = 
        setpt=(ezca['OMC-READOUT_X0_OFFSET']**2) / (ezca['OMC-READOUT_XF_OFFSET']**2)
        ezca['OMC-READOUT_PREF_OFFSET'] = (setpt+ezca['OMC-READOUT_CD_OUTPUT'])*ezca['OMC-READOUT_TRAVG']/np.median(z[1].data)

        # calculate the output of the OMC-READOUT path so we can match the gain to DARM_IN1
        OMC_OUT = z[1].data * ezca['OMC-READOUT_PREF_OUTPUT'] / ezca['OMC-READOUT_TRAVG'] /2 * ezca['OMC-READOUT_XF_OUTPUT']**2 / ezca['OMC-READOUT_X0SAT'] * ezca['OMC-READOUT_ERR_GAIN']

        if z[0].sample_rate == z[1].sample_rate:
            fs = z[0].sample_rate
        else:
            log('Sample rates are not the same!!!')
            fs = z[0].sample_rate

        ### compute transfer function and coherence in the 1-15Hz band
        # length of each FFT, 3 second strides
        nfft = int(fs / 0.33) 
        # calculate PSDs
        Pxx,fr = mlab.psd(DARM_IN, NFFT=nfft, Fs=fs, noverlap=nfft/2)
        Pyy,fr = mlab.psd(OMC_OUT, NFFT=nfft, Fs=fs, noverlap=nfft/2)
        # find the correct bin index
        idx1 = np.argmin(abs(fr-1.0))
        idx2 = np.argmin(abs(fr-15.0))

        Pxy,fr = mlab.csd(DARM_IN, OMC_OUT, NFFT=nfft, Fs=fs, noverlap=nfft/2)

        # compute TF and coherence at the bin
        TFxy = Pxy[idx1:idx2] / Pxx[idx1:idx2]
        COHxy = abs(Pxy[idx1:idx2])**2 / (Pxx[idx1:idx2] * Pyy[idx1:idx2])

        # only use data with good coherence
        good_idx = np.where(COHxy > 0.9)[0]

        # Print some results
        TFmag = np.median(abs(TFxy[good_idx]))
        TFphase = cmath.phase(np.median(TFxy[good_idx]))*180/np.pi
        log('Complex TF parameters (magnitude and phase):')
        log(TFmag)
        log(TFphase)

        correction_sign = 1.0
        if np.median(abs(TFphase)) > 150:
            correction_sign = -1.0
        elif np.median(abs(TFphase)) < 30:
            correction_sign = 1.0
        else:
            log('TF phase is weird!!')
        
        #scale_start = ezca['OMC-READOUT_SCALE_OFFSET']
        scale_start = ezca['OMC-READOUT_ERR_GAIN']
        log('Scale start:')

        log(scale_start)
        scale_correction = correction_sign * scale_start / TFmag
        log('Scale calculation:')
        log(scale_correction)

        ezca['OMC-READOUT_ERR_GAIN'] = scale_correction

        time.sleep(1)

        ezca['LSC-OMC_DC_OFFSET']=-ezca['LSC-DARM1_OFFSET']  #if you take into account the power normalization, the gain from LSC-OMC_DC to DARM IN1 is 1
'''
##########################################################
# Final state
class READY_FOR_HANDOFF(GuardState):
    index = 500
    request = True

    #@assert_light_on_qpds
    #@assert_omc_locked
    def main(self):

        # Temporarily remove dc check logic AJM 201211
        #self.check_for_dc = True
        self.check_for_dc = False

    #@assert_light_on_qpds
    #@assert_omc_locked
    def run(self):

        if self.check_for_dc:
            if not ready_for_dc():
                log('Actually, not locked on carrier')
                log('Either lock manually, or rerun the OMC guardian by requesting DOWN')
                unlock_omc()
                turn_off_omc_asc()
                reset_pzt()
                return 'MANUAL_LOCKING'

        self.check_for_dc = False
        #notify('OMC is ready to go! :-D')  #Notification are for failures only! No news is good news!
        return True

##########################################################
def gen_WHITENING_CHANGE(action='add'):
    class WHITENING_CHANGE(GuardState):
        request = False

        '''
        Change the whitening state of the DCPDs.
        This state is gentle enough to be used in full lock.
        '''

        def main(self):
            # Get DCPD filters and gains, and set ramp times
            dcpdA = ezca.get_LIGOFilter('OMC-DCPD_A')
            dcpdB = ezca.get_LIGOFilter('OMC-DCPD_B')
            dcpdA.TRAMP.put(20)
            time.sleep(0.1)
            dcpdB.TRAMP.put(20)
            time.sleep(0.1)
            dcpdA_gain = dcpdA.GAIN.get()
            dcpdB_gain = dcpdB.GAIN.get()
            # Generate the list of whitening channels
            dcpdA_whstages = ['OMC-DCPD_A_WHITEN_SET_{}'.format(ii+1)
                            for ii in range(3)]
            dcpdB_whstages = ['OMC-DCPD_B_WHITEN_SET_{}'.format(ii+1)
                            for ii in range(3)]

            if ezca['GRD-ISC_LOCK_STATE_N'] > 500: # If we are past DC Readout Transition, state 500
                # Switch DARM over entirely to A, then change whitening on B
                dcpdA.GAIN.put(2*dcpdA_gain)
                dcpdB.GAIN.put(0)
                time.sleep(1)
                while dcpdA.is_gain_ramping() or dcpdB.is_gain_ramping():
                    time.sleep(1)
                whitening_change_helper(dcpdB, dcpdB_whstages, action)
                time.sleep(1)
                # Switch DARM over entirely to B, then change whitening on A
                dcpdA.GAIN.put(0)
                dcpdB.GAIN.put(2*dcpdB_gain)
                time.sleep(1)
                while dcpdA.is_gain_ramping() or dcpdB.is_gain_ramping():
                    time.sleep(1)
                whitening_change_helper(dcpdA, dcpdA_whstages, action)
                time.sleep(1)
            else:  # If we're at Transition or before (so we're not using DCPDs for DARM), just swap the whitening, no gain ramping
                whitening_change_helper(dcpdA, dcpdA_whstages, action)
                whitening_change_helper(dcpdB, dcpdB_whstages, action)

            # Switch DARM to equal control of A and B
            dcpdA.GAIN.put(dcpdA_gain)
            dcpdB.GAIN.put(dcpdB_gain)
            return True 

        def run(self):
            if ezca['OMC-DCPD_A_GAIN'] != ezca['OMC-DCPD_B_GAIN']:
                notify('DCPD gains not equal!')
            else:
                return 'READY_FOR_HANDOFF'
    return WHITENING_CHANGE


ADD_WHITENING    = gen_WHITENING_CHANGE('add')
REMOVE_WHITENING = gen_WHITENING_CHANGE('remove')

##################################################
# EDGES 
##################################################

edges = [

# LLO slow scan 
    ('INIT',                'IDLE'),
    ('IDLE',                'DOWN'),
    ('DOWN',                'WAIT_FOR_AS_LIGHT'),
    #('WAIT_FOR_AS_LIGHT',   'PREP_OMC_SCAN'),
    ('WAIT_FOR_AS_LIGHT',   'ASC_QPD_ON'),
    ('ASC_QPD_ON',          'PREP_OMC_SCAN'),
    ('PREP_OMC_SCAN',       'OMC_QUICKLOCK'),
    ('PREP_OMC_SCAN',       'FIND_CARRIER'),
    ('FIND_CARRIER',        'OMC_LSC_ON'),
    ('OMC_LSC_ON',          'OMC_LOCKED'),
    ('OMC_QUICKLOCK',       'CARRIER_CHECK'),
    ('CARRIER_CHECK',       'OMC_LOCKED'),
    ('OMC_LOCKED',          'ASC_DITHER_ON'),
#    ('ASC_DITHER_ON',       'CARRIER_CHECK'),
#    ('CARRIER_CHECK',       'READY_FOR_HANDOFF'),
    ('ASC_DITHER_ON',       'READY_FOR_HANDOFF'),
    ('DOWN',                'MANUAL_LOCKING', 11), 
    ('MANUAL_LOCKING',      'READY_FOR_HANDOFF'),

# LHO import
#    ('INIT',                'DOWN'),
#    ('DOWN',                'WAIT_FOR_AS_LIGHT'),
#    ('WAIT_FOR_AS_LIGHT',   'PREP_OMC_SCAN'),
#    ('PREP_OMC_SCAN',       'FIND_CARRIER'),
#    ('FIND_CARRIER',        'OMC_LSC_ON'),
#    ('OMC_LSC_ON',          'OMC_LOCKED'),
#    ('OMC_LOCKED',          'READY_FOR_HANDOFF'),


#    ('INIT',                'DOWN'),
#    ('DOWN',                'OFF_RESONANCE'),
#    ('DOWN',                'WAIT_FOR_AS_LIGHT'),
#    ('WAIT_FOR_AS_LIGHT',   'ASC_QPD_ON'),
#    ('ASC_QPD_ON',          'PREP_OMC_SCAN'),
#    ('PREP_OMC_SCAN',       'FIND_CARRIER'),
#    ('FIND_CARRIER',        'OMC_LSC_ON'),
#    ('OMC_LSC_ON',          'OMC_LOCKED'),
#    ('OMC_LOCKED',          'DITHER_ON'),
#    ('DITHER_ON',           'TUNE_OFFSETS'),
#    ('TUNE_OFFSETS',        'READY_FOR_HANDOFF'),    
#    ('READY_FOR_HANDOFF',   'TUNE_OFFSETS'),
#    ('READY_FOR_HANDOFF',   'ADD_WHITENING'),
#    ('ADD_WHITENING',       'READY_FOR_HANDOFF'),
#    ('READY_FOR_HANDOFF',   'REMOVE_WHITENING'),
#    ('REMOVE_WHITENING',    'READY_FOR_HANDOFF'),
    ]

