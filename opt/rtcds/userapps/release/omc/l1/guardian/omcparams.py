######## H1 OMC Guardian Parameters

# threshold for locking on CR_TEM00
lsc_trigger_threshold = 4.0 	#AJM20200224 need to review this. 1.0

# the gain of the lsc loop
lsc_gain = 4.0

# the gain of the asc loops
asc_gain = 6.0

# the dither frequency for the lsc loop
lsc_dither_freq = 4500.1

# the filters to turn on in the LSC loop, not including boost (FM1) and integrator (FM2)
lsc_filters = [4,5,6,8,10]

# the time scale of the PZT ramp
pzt_ramp_time = 15

# step size of pzt sweep
pzt_step_size = 15

# value to reset to after leaving the PZT range
pzt_reset_value = 10

# acceptable range of H1:OMC-PZT2_OFFSET
pzt_range = [-50, 50]

# size of jump to put into PZT when locked on the wrong mode
pzt_jump_size = 1

# the ASC master gain when in QPD alignment
asc_master_gain_qpd = 1

# the ASC master gain when in dither alignment
asc_master_gain_dither = 0.02

# this is the minimum power required on the QPD in order to enable alignment
qpd_power_threshold = 50

# time scale to for offloading dither outputs to TT
dither_offload_time = 10

# desired camera exposure setting
camera_exposure = 10000

# OMC trans Camera channel name prefix
camera_channel = 'CAM-OMC_TRANS'

#####
# parameters to find carrier with "direct" sweep around last carrier
# offset value
find_carrier_direct_normthreshold = 0.5

#####
# parameters to find carrier with full PZT sweep for sidebands
find_carrier_sweep = [-30, 20]
find_carrier_sweep_step = 0.02
find_carrier_sideband_detection_normthreshold = 3
find_carrier_sideband_full_normthreshold = 5

carrier_sideband_normthreshold = 10

#####
# cavity mode properties
mode_delta_sideband = 8.3
mode_fsr_offset = 43
mode_carrier_offset_error = 3

# DARM offset threshold.
darm_offset_LOLO = 1e-6 # lower threshold
darm_offset_HIHI = 1e-4 # upper threshold



# OMC scan settings
scan_tramp = 60 # scan time scale [sec]
scan_vstart = -25	#-50   # initial voltage for scan in [V]
scan_vramp = 50# voltage to ramp in [V] # Made larger by 5 so we don't have to keep changing the vstart - 1Apr2016 JCD

# OMC peak finding parameters
num_smalldataset = 7 # number of the segments that the whole scan data is divided into

