# Docstring:
"""
Averaging class that uses epics, i.e. has no dependence on NDS.

Author: Adam Mullavey.
Date: 2023-03-24
Contact: adam.mullavey@ligo.org

Modified: 2023-03-24 (Created)
Modified: 2023-04-03 (Added a simple averaging function ezavg, for skipping the
                      instantiation, mostly for when you only want to do once)
Modified: 2024-04-30 (Vladimir Bossilkov: Added optional to use median instead of mean.)
"""


import numpy as np
import math
import gpstime


class EzAvg(object):

    def __init__(self,ezca,tAvg,chans, movingAverage = True, tDelay=0):
        self._ezca = ezca
        self._chans = chans
        self._tAvg = tAvg + tDelay
        self._movingAverage = movingAverage

        self._first_point = True
        self._done = False

        self._multiple_chans = isinstance(chans,list)

        if self._multiple_chans:
            self._buffArray = np.zeros((len(chans),1))
        else:
            self._buffArray = np.array([0])

        # Points to leave off end of array for averaging, negative for array handling purposes
        if tDelay == 0:
            self._nDelay = None
        else:
            self._nDelay = -1*math.floor(tDelay/0.0625)



    def clear_buff(self):
        if self._multiple_chans:
            self._buffArray = np.zeros((len(self._chans),1))
        else:
            self._buffArray = np.array([0])



    def ezAvg(self,stddev=False,median=False):

        if self._first_point:
            self._tstop = gpstime.gpsnow() + self._tAvg
            self._tnext = gpstime.gpsnow()
            self._first_point = False

        # Only add (or drop) data at 16Hz rate
        if gpstime.gpsnow() >= self._tnext:
            self._tnext = gpstime.gpsnow() + 0.0625

            # If averaging time reached, drop off first data point
            if gpstime.gpsnow() > self._tstop:
                self._done = True
                if self._multiple_chans:
                    self._buffArray = self._buffArray[0:len(self._chans),1:]
                else:
                    self._buffArray = self._buffArray[1:]

            # Add new data
            if self._multiple_chans:
                new_data = np.array([])
                for chan in self._chans:
                    new_data = np.append(new_data,self._ezca[chan])
                self._buffArray = np.append(self._buffArray,new_data.reshape(len(self._chans),1),axis=1)
            else:
                self._buffArray = np.append(self._buffArray,ezca[self._chans])



        if self._done: 
            avgs = []
            if self._multiple_chans:
                for ii in range(len(self._chans)):
                    if stddev:
                        if median:
                            avgs.append((np.median(self._buffArray[ii,0:self._nDelay]),np.std(self._buffArray[ii,0:self._nDelay])))
                        else:
                            avgs.append((np.mean(self._buffArray[ii,0:self._nDelay]),np.std(self._buffArray[ii,0:self._nDelay])))
                    else:
                        if median:
                            avgs.append(np.median(self._buffArray[ii,0:self._nDelay]))
                        else:
                            avgs.append(np.mean(self._buffArray[ii,0:self._nDelay]))
            else:
                if stddev:
                    if median:
                        avgs = (np.median(self._buffArray[0:self._nDelay]),np.std(self._buffArray[0:self._nDelay]))
                    else:
                        avgs = (np.mean(self._buffArray[0:self._nDelay]),np.std(self._buffArray[0:self._nDelay]))
                else:
                    if median:
                        avgs = np.median(self._buffArray[0:self._nDelay])
                    else:
                        avgs = np.mean(self._buffArray[0:self._nDelay])

            if not self._movingAverage:
                self._first_point = True
                self._done = False
                self.clear_buff()

            return True, avgs

        else:
            return False, None


# Simple function for calling average once (avoid instantiation)
def ezavg(ezca,tAvg,chans,stddev=False,median=False):

    simpleAvg = EzAvg(ezca,tAvg,chans,movingAverage = False)
    done = False
    while not done:
        [done,avgs] = simpleAvg.ezAvg(stddev,median)

    return avgs
